# Turtl Backup

`tbak.py`: 
```
Takes a turtl-backup.json file, exported from turtl
and outputs it into various note files, creating directories by board name

:env BACKUP_DIR - local directory, defaults to 'backup'
:env TURTL_BACKUP_FILEPATH - path/to/turtl-backup.json, defaults to 'turtl-backup.json'
```

the other .py files are from https://github.com/JulienPalard/turtl-backup
