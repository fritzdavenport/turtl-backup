"""Takes a turtl-backup.json file, exported from turtl
and outputs it into various note files, creating directories by board name

:env BACKUP_DIR - local directory, defaults to 'backup'
:env TURTL_BACKUP_FILEPATH - path/to/turtl-backup.json, defaults to 'turtl-backup.json'
"""

import json
import os
from pprint import pprint


def create_folder(s):
    print(f'creating: {s} ...')
    if not os.path.exists(s):
        os.mkdir(s)


PREFIX = os.getenv("BACKUP_DIR", 'backup')
create_folder(PREFIX)
with open(os.getenv("TURTL_BACKUP_FILEPATH", "turtl-backup.json")) as fin:
    j = json.loads(fin.read())
    board_to_name = {board['id']: board['title'] for board in j['boards']}
    board_to_name['unsorted'] = 'unsorted'
    for board in board_to_name.values():
        create_folder(f'{PREFIX}/{board}')

    for note in j['notes']:
        # pprint(note)
        board = board_to_name[note.get('board_id', 'unsorted') or 'unsorted']

        try:
            title = note['title'].replace(" ", "_").replace('/', '+').lower()+'.md'
        except KeyError:
            title = 'untitled.md'

        if note['type'] == 'text':
            title = f"{PREFIX}/{board}/{title}"
            with open(title, 'w') as fout:
                print(f'creating: {title}')
                fout.write(note['text'])
        else:
            print(f'note "{note["title"]}" has type {note["type"]} , skipping... ')
